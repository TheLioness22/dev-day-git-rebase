<html>
  <head>
    <title>Dev Day: Git Rebase</title>
  </head>
  <body>
    <h1>Dev Day: Git Rebase</h1>
    <ul>
      <li>Something</li>
      <li>Another thing</li>
      <li>One more thing</li>
    </ul>
    <table>
      <thead>
        <tr>
          <th>Make</th>
          <th>Model</th>
          <th>Color</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Ford</td>
          <td>Focus</td>
          <td>Silver</td>
        </tr>
        <tr>
          <td>Tesla</td>
          <td>Model S</td>
          <td>Black</td>
        </tr>
        <tr>
          <td>Toyota</td>
          <td>RAV-4</td>
          <td>Red</td>
        </tr>
        <tr>
          <td>Honda</td>
          <td>Civic</td>
          <td>Blue</td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
